import pyb

uart = pyb.UART(3, 2400)

tag_id = '' # ID of the tag that is near the sensor right now

while True:
    if uart.any():
        msg = uart.readline().decode()[:-2] # removing line endings

        if len(msg) > 0:
            if tag_id == '':
                tag_id = msg[:9]
                print('tag_id:' + tag_id)
            elif len(msg) == 9: # short msg means tag is removed
                tag_id = ''

    pyb.delay(100)
