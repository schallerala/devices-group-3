import time, serial, requests

# workaround to flush the serial connection
acm0 = serial.Serial("/dev/ttyACM0", timeout = 1)
acm0.readline()
acm0.readline()
acm0.timeout = 0
while True:
    if len(acm0.readline().decode()) == 0:
        break

# main loop
print('Ready to work!')
while True:
    msg = acm0.readline().decode()[:-2] # removing line endings
    if msg[:7] == 'tag_id:': 
        try:
            response = requests.post('http://127.0.0.1:80/auth/login', { 'tag_id': msg[7:] })
        except Exception as e:
            print('Error: {0}'.format(e))
        else:
            print(response)
    time.sleep(0.1)
