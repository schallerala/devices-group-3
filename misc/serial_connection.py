import serial
import json


class MyEncoder(json.JSONEncoder):
    def default(self, obj):
        return obj.__dict__


class COM_Connection():
    connection = None

    def __init__(self, port, speed, parser_prefix="-->", parser_suffix="<--\r\n"):
        self.connection = serial.Serial(port=port, baudrate=speed)

        if type(parser_prefix) is str:
            parser_prefix = parser_prefix.encode()

        if type(parser_suffix) is str:
            parser_suffix = parser_suffix.encode()

        if not parser_suffix.endswith(b'\r\n'):
            print("\r\n!!! WARNING !!!\r\n")
            print("the parser suffix, should end with a new line character \\r\\n")

    def start(self):
        if not self.connection.isOpen():
            self.connection.open()

    def stop(self):
        if self.connection.isOpen():
            self.connection.close()

    def write(self, data):
        json_encode = json.dumps(data, cls=MyEncoder)
        self.connection.write(json_encode.encode())

    def read(self):
        """Read and return one line from the stream.

        :return: bytes
        """
        return json.load(self.connection)

    def inWaiting(self):
        return self.connection.inWaiting()