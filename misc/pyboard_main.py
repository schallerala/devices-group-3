from pyb import USB_VCP
from parser import Write_Parser, Read_Parser

def main():
    prefix = "-->"
    suffix = "<--\r\n"

    write_parser = Write_Parser(prefix, suffix)

    usb = USB_VCP()
    usb.write(
        write_parser.parse(str(pyb.rng() % 10000))
    )
    pyb.delay(200)

    # read from the usb for an answer

while 1:
    main()