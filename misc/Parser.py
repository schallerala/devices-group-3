class Parser():
    __prefix = None
    __suffix = None

    def __init__(self, prefix=None, suffix=None):
        if prefix is not None and type(prefix) == str:
            prefix = prefix.encode()

        if suffix is not None and type(suffix) == str:
            suffix = suffix.encode()

        self.__prefix = prefix
        self.__suffix = suffix

    def parse(self, to_parse):
        pass

    def get_prefix(self):
        return self.__prefix

    def get_suffix(self):
        return self.__suffix


class Read_Parser(Parser):
    def __init__(self, prefix, suffix):
        Parser.__init__(self, prefix, suffix)

    def parse(self, to_parse):
        is_string = type(to_parse) is str
        if is_string:
            to_parse = to_parse.encode()

        parsed = self.translate_prefix(to_parse)
        parsed = self.translate_suffix(parsed)

        if is_string:
            parsed = parsed.decode()
        return parsed

    def translate_prefix(self, to_translate):
        if self.get_prefix() is None:
            return to_translate

        return to_translate.replace(self.get_prefix(), b'')

    def translate_suffix(self, to_translate):
        if self.get_suffix() is None:
            return to_translate

        return to_translate.replace(self.get_suffix(), b'')


class Write_Parser(Parser):
    def __init__(self, prefix, suffix):
        Parser.__init__(self, prefix, suffix)

    def parse(self, to_parse):
        is_string = type(to_parse) is str
        if is_string:
            to_parse = to_parse.encode()

        parsed = self.prefix(to_parse)
        parsed = self.suffix(parsed)

        if is_string:
            parsed = parsed.decode()
        return parsed

    def prefix(self, to_parse):
        if self.get_prefix() is None:
            return to_parse
        return self.get_prefix() + to_parse

    def suffix(self, to_parse):
        if self.get_suffix() is None:
            return to_parse
        return to_parse + self.get_suffix()
