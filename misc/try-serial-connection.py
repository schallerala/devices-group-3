import serial
import time

ser = serial.Serial(
    port='/dev/tty.usbmodemFD122', baudrate=115200
)

if ser.isOpen():
    ser.close()

ser.open()
print("serial is open?", ser.isOpen())

print('Enter your commands below.\r\nInsert "exit" to leave the application.')

while 1 :
    # get keyboard input
    user_input = input(">> ")
    if user_input == 'exit':
        ser.close()
        exit()
    else:
        # send the character to the device
        ser.write(
            str(user_input + '\r\n').encode()
        )
        out = ''
        # let's wait one second before reading output (let's give device time to answer)
        time.sleep(1)
        while ser.inWaiting() > 0:
            out += ser.read(1).decode()

        print("output:")
        print(out)