create TABLE accounts (
    id int(11) not null auto_increment,
    tag_id varchar(66) not null,
    full_name varchar(60) not null,
    balance double not null default '0',
    created_at timestamp not null default CURRENT_TIMESTAMP,
    updated_at timestamp not null default '0000-00-00 00:00:00',
    login_timeout timestamp null,
    PRIMARY KEY (id)
);
CREATE UNIQUE INDEX unique_id ON accounts (id ASC);
CREATE UNIQUE INDEX unique_tag_id ON accounts (tag_id ASC);

create TABLE products (
    id int(11) not null auto_increment,
    description varchar(60) not null,
    price double not null,
    created_at timestamp not null default CURRENT_TIMESTAMP,
    updated_at timestamp not null default '0000-00-00 00:00:00',
    PRIMARY KEY (id)
);
CREATE UNIQUE INDEX unique_id ON products (id ASC);

create TABLE operations (
    id int(11) not null auto_increment,
    account_id int(11) not null,
    product_id int(11),
    value double not null,
    created_at timestamp not null default CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    FOREIGN KEY (account_id) REFERENCES accounts (id),
    FOREIGN KEY (product_id) REFERENCES products (id)
);
CREATE UNIQUE INDEX unique_id ON operations (id ASC);
