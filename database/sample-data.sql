
INSERT INTO accounts(tag_id,full_name,balance) VALUES ('8500A5AF2','John Miller','20');
INSERT INTO accounts(tag_id,full_name,balance) VALUES ('BCDEFGHIJ','Peter Robson','15');
INSERT INTO accounts(tag_id,full_name,balance) VALUES ('CDEFGHIJK','Alex Cortana','15');

INSERT INTO products(description,price) VALUES ('Dark coffee','2.5');
INSERT INTO products(description,price) VALUES ('Black tea','1.5');
INSERT INTO products(description,price) VALUES ('Fazer dark chocolate','3.5');
INSERT INTO products(description,price) VALUES ('Green tea','2.5');
INSERT INTO products(description,price) VALUES ('White tea','2.2');
INSERT INTO products(description,price) VALUES ('Milk','1.5');
INSERT INTO products(description,price) VALUES ('Cookie','1.5');
INSERT INTO products(description,price) VALUES ('Snickers','2.0');
INSERT INTO products(description,price) VALUES ('Fazer biscuit','3.5');
INSERT INTO products(description,price) VALUES ('Fazer milk chocolate','3.2');
INSERT INTO products(description,price) VALUES ('Mint chocolate','3.2');
INSERT INTO products(description,price) VALUES ('Ice cream','2.2');
INSERT INTO products(description,price) VALUES ('Chocolate milk','3.5');
INSERT INTO products(description,price) VALUES ('White coffee','3.5');
INSERT INTO products(description,price) VALUES ('Sandwich','5.5');
INSERT INTO products(description,price) VALUES ('Cappuccino','3.2');
INSERT INTO products(description,price) VALUES ('Latte','2.7');
INSERT INTO products(description,price) VALUES ('French fries','3.5');
INSERT INTO products(description,price) VALUES ('Hamburger','6.5');
INSERT INTO products(description,price) VALUES ('Fried chicken','5.5');

INSERT INTO operations(account_id,product_id,value) VALUES (1,3,'5');
INSERT INTO operations(account_id,product_id,value) VALUES (2,1,'3');
INSERT INTO operations(account_id,product_id,value) VALUES (3,2,'4');
INSERT INTO operations(account_id,product_id,value) VALUES (1,4,'2');
