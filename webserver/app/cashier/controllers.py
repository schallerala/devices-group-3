from flask import Blueprint, render_template
from flask import json

from app import socketio, db

from flask_socketio import emit

from .models import Product, Operation

from ..auth.controllers import update_login_timeout, logout_account, find_login_account

# Define the blueprint: 'auth', set its url prefix: app.url/auth
cashier = Blueprint('cashier', __name__, url_prefix='/cashier')


@cashier.route("/")
def index():
    products = Product.query.all()
    serialized_products = []
    for product in products:
        serialized_products.append(product.serialize())

    found_account = find_login_account()
    if found_account is not None:
        serialized_account = found_account.serialize()
    else:
        serialized_account = None

    return render_template('cashier/index.html',
                           product_list=products,
                           serialized_cashier_product=json.dumps(serialized_products),
                           serialized_account=json.dumps(serialized_account))


@socketio.on('extend login')
def on_action():
    if not update_login_timeout():
        emit('account logout', "Timeout")


@socketio.on('logout account')
def on_logout_action():
    logout_account()
    emit('account logout', "Confirm logout out")


@socketio.on('pay')
def on_pay_action(raw_productList):
    logged_in_account = find_login_account()
    if logged_in_account is None:
        print("can't pay, no user logged in")
        emit('error payment', "User not logged in anymore")
        return

    product_quantity = {}
    productIds = []
    for product in raw_productList:
        productIds.append(product['productId'])
        product_quantity[product['productId']] = product['quantity']

    productList = Product.query.filter(Product.id.in_(productIds)).all()
    total_price = 0
    for product in productList:
        value = - product.price * product_quantity[product.id]
        new_operation = Operation(value, logged_in_account, product)
        db.session.add(new_operation)
        total_price += new_operation.value

    logged_in_account.balance += total_price

    db.session.commit()
    logout_account()
    emit("payed", logged_in_account.full_name + " payed " + str(-1 * total_price) + "€")
    emit("account logout", "Paid")