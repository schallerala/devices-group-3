from app import db
from ..auth.models import Account

# Define a base model for other database tables to inherit
class Base(db.Model):
    __abstract__  = True

    id           = db.Column(db.Integer, primary_key=True)
    created_at   = db.Column(db.DateTime,  default=db.func.current_timestamp())
    updated_at   = db.Column(db.DateTime,  default=db.func.current_timestamp(),
                                           onupdate=db.func.current_timestamp())


    def save(self):
        db.session.commit()

# Define a User model
class Product(Base):

    __tablename__ = 'products'

    description = db.Column(db.String)
    price = db.Column(db.Float)

    # New instance instantiation procedure
    def __init__(self, description, price):
        self.description = description
        self.price = price


    def __repr__(self):
        return '<Product %s>' % (self.description)

    def serialize(self):
        return {
            '_class': 'Product',
            'id': self.id,
            'description': self.description,
            'price': self.price
        }


class Operation(Base):

    __tablename__ = 'operations'

    account_id = db.Column(db.Integer, db.ForeignKey('accounts.id'), nullable=False)
    account = db.relationship('Account')

    product_id = db.Column(db.Integer, db.ForeignKey('products.id'), nullable=True)
    product = db.relationship('Product')

    value = db.Column(db.Float)

    updated_at = None

    def __init__(self, value, account: Account, product: Product = None):
        self.value = value
        self.account = account
        self.product = product

    def __repr__(self):
        return '<Operation %s>' % (self.id)
