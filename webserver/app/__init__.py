# Import flask and template operators
from flask import Flask, render_template

import atexit

# Import SQLAlchemy
from flask.ext.sqlalchemy import SQLAlchemy

from flask_socketio import SocketIO

from apscheduler.schedulers.background import BackgroundScheduler

# Define the WSGI application object
app = Flask(__name__)

# Configurations
app.config.from_object('config')

# Define the database object which is imported
# by modules and controllers
db = SQLAlchemy(app)

# Define socketio object to use in our paying view
# To know when a tag in found
socketio = SocketIO(app)

# Global scheduler that we are gonna use to check the account timeout
scheduler = BackgroundScheduler()
scheduler.start()
atexit.register(lambda: scheduler.shutdown(wait=False))

# Import a module / component using its blueprint handler variable (mod_auth)
from app.auth.controllers import authentication as authentication_module
from app.cashier.controllers import cashier as cashier_module

# Register blueprint(s)
app.register_blueprint(authentication_module)
app.register_blueprint(cashier_module)

# Sample HTTP error handling
@app.errorhandler(404)
def not_found(error):
    return render_template('404.html'), 404

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/show-routes')
def show_routes():
    return str(app.url_map)