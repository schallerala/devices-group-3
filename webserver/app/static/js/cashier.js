(function ($, angular, io, serialized_cashier_product) {
    function Account (id, full_name, balance) {
        var rawAccount;
        if (typeof id == "object") {
            rawAccount = id;
        }

        this.id = rawAccount && rawAccount.id || id;
        this.full_name = rawAccount && rawAccount.full_name || full_name;
        this.balance = rawAccount && rawAccount.balance || balance || 0;
    }

    function Product (id, description, price) {
        var rawProduct;
        if (typeof id == "object") {
            rawProduct = id;
        }

        this.id = rawProduct && rawProduct.id || id;
        this.description = rawProduct && rawProduct.description || description;
        this.price = rawProduct && rawProduct.price || price || 0;
    }

    function BasketEntry (product) {
        if (! product instanceof Product) product = new Product(product);
        this.id = product.id; // To make it quicker to find the basketEntry for a specific product
        this.product = product;
        this.quantity = 1;
        this.subTotal = product.price;
    }

    BasketEntry.prototype.add = function (quantity) {
        quantity = quantity || 1;
        this.quantity += quantity;
        this.subTotal += this.product.price * quantity;
    };

    BasketEntry.prototype.remove = function (quantity) {
        quantity = quantity || -1;
        this.add(quantity);
    };

    /**
     * Function that returns an object that we will return to the server, when we pay
     */
    BasketEntry.prototype.simple = function () {
        return {
            productId: this.id,
            quantity: this.quantity
        };
    };

    function Basket (account) {
        this.basketEntries = [];
        this.total = 0;
        this.linkedAccount = null;
        this.newLinkedAccountBalance = null;

        // to link an account and check if we can add the product
        // without having a negative balance
        if (account && account instanceof Account) {
            this.linkedAccount = account;
            this.newLinkedAccountBalance = this.linkedAccount.balance;
        }
    }

    Basket.prototype.linkToAccount = function (account) {
        this.linkedAccount = account;
        this.newLinkedAccountBalance = account.balance;
    };

    Basket.prototype.unlinkAccount = function () {
        this.linkedAccount = null;
        this.newLinkedAccountBalance = null;
    };

    Basket.prototype.updateNewAccountBalance = function () {
        if (this.linkedAccount) {
            this.newLinkedAccountBalance = this.linkedAccount.balance - this.total;
        }
    };

    Basket.prototype.findEntry = function (product) {
        var productId = typeof product == "object" && product.id || product,
            foundBasketEntry;

        $.each(this.basketEntries, function (index, basketEntry) {
            if (basketEntry.id === productId) {
                foundBasketEntry = basketEntry;
                return false;
            }
        });

        return foundBasketEntry;
    };

    Basket.prototype.addEntry = function (product) {
        var existingBasketEntry = this.findEntry(product);

        // check account integrity
        if (this.linkedAccount && this.linkedAccount.balance < this.total + product.price) {
            console.info("Can't buy this, not enough money left");
            return;
        }

        if ( ! this.linkedAccount) return;

        if (existingBasketEntry) {
            existingBasketEntry.add();
        } else this.basketEntries.push(new BasketEntry(product));

        this.total += product.price;

        this.updateNewAccountBalance();
    };

    Basket.prototype.removeEntry = function (product) {
        var existingBasketEntry = this.findEntry(product),
            existingBasketEntryIndex;

        if ( ! existingBasketEntry) {
            alert('Couldn\'t decrease quantity for product: ' + product.description);
        }

        existingBasketEntry.remove();

        if (existingBasketEntry.quantity === 0) {
            existingBasketEntryIndex = this.basketEntries.indexOf(existingBasketEntry);
            this.basketEntries.splice(existingBasketEntryIndex, 1);
        }

        // can't use the product object that we got in argument,
        // since it could only be the product id
        this.total -= existingBasketEntry.product.price;

        this.updateNewAccountBalance();
    };

    Basket.prototype.getProductList = function () {
        var productList = [];
        $.each(this.basketEntries, function (index, productEntry) {
            productList.push(productEntry.simple());
        });
        return productList;
    };

    Basket.prototype.empty = function () {
        this.basketEntries = [];
        this.total = 0;
    };

    var notify_template =
        '<div data-notify="container" class="col-xs-11 col-sm-10 col-md-8 alert-notification alert alert-{0}" role="alert">' +
            '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
            '<span data-notify="title">{1}</span> ' +
            '<span data-notify="message">{2}</span>' +
            '<div class="progress" data-notify="progressbar">' +
               '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
            '</div>' +
        '</div>';

    var app = angular.module('cashier', [], function($interpolateProvider) {
        $interpolateProvider.startSymbol('[[');
        $interpolateProvider.endSymbol(']]');
    });

    app.factory('processed-products',
        function () {
            var raw_products = JSON.parse(serialized_cashier_product),
                products = [];
            $.each(raw_products, function (index, raw_product) {
                products.push(new Product(raw_product));
            });

            return products;
        }
    );

    app.factory('processed-account',
        function () {
            var rawAccount = JSON.parse(serialized_account);
            return rawAccount && new Account(rawAccount) || null;
        }
    );

    app.controller(
        'cashierController',
        ['$scope', 'processed-products', 'processed-account',
            function ($scope, processedProducts, processedAccount) {
                var self = this,
                    socketio = io.connect();

                socketio.on('account login', function (data) {
                    var raw_account = JSON.parse(data);
                    self.account = new Account(raw_account);
                    self.basket.linkToAccount(self.account);
                    $scope.$digest(); // force to evaluate every "watchers"

                    $('.item').removeClass('disabled');
                });
                socketio.on('account logout', function () {
                    self.basket.empty();
                    self.basket.unlinkAccount();
                    self.account = null;
                    $scope.$digest(); // force to evaluate every "watchers"

                    $('.item').addClass('disabled');
                });
                socketio.on('payed', function (message) {
                    $.notify(message, {
                        showProgressbar: true,
                        delay: 1200,
                        timer: 80,
                        mouse_over: 'pause',
                        template: notify_template
                    });
                });

                this.account = processedAccount;
                this.products = processedProducts;
                this.basket = new Basket(this.account);

                if ( ! this.account) {
                    $('.item').addClass('disabled');
                }


                this.logoutAccount = function () {
                    socketio.emit('logout account');
                };

                this.onClickAddProductInBasket = function (productId) {
                    var foundProduct = findProductById(productId);
                    if ( ! foundProduct) {
                        alert('Can\'t buy this product');
                        return;
                    }

                    this.basket.addEntry(foundProduct);
                    extendLogin();
                };

                this.onDblClickRemoveProductInBasket = function (productId) {
                    this.basket.removeEntry(productId);
                    extendLogin();
                };

                this.onClickPay = function () {
                    if ( ! this.account) return;
                    var productList = this.basket.getProductList();

                    socketio.emit('pay', productList);
                };

                this.showNewBalance = function () {
                    return this.basket.total > 0;
                };

                function findProductById (productId) {
                    var foundProduct = null;
                    $.each(self.products, function (index, product) {
                        if (productId === product.id) {
                            foundProduct = product;
                            return false;
                        }
                    });

                    if ( ! foundProduct) {
                        console.warn("didn't find product with id: " + productId);
                    }
                    return foundProduct;
                }

                function extendLogin () {
                    socketio.emit('extend login');
                }
            }
        ]
    );
})(jQuery, angular, io, serialized_cashier_product);