from flask import Blueprint, request, make_response, current_app, json
import datetime

# Import module models (i.e. User)
from .models import Account
from app import socketio, scheduler
import pymysql, requests


def login_account(account_to_login: Account):
    account_to_login.login_timeout = \
        datetime.datetime.now() + datetime.timedelta(seconds=get_account_timeout())
    account_to_login.save()


def check_login_timeout_job():
    conn = pymysql.connect(host='localhost', user='root', db='group3_devices')
    cursor = conn.cursor()
    cursor.execute('select id from accounts where login_timeout < now()')
    result = cursor.fetchone()
    cursor.close()
    if result is not None:
        cursor = conn.cursor()
        cursor.execute('update accounts set login_timeout = null where id = %s', (result[0],))
        cursor.close()
        conn.commit()
        requests.get('http://localhost:80/auth/logout')
    conn.close()


def get_account_timeout():
    return current_app.config['ACCOUNT_TIMEOUT']


def check_login_timeout_integrity() -> bool:
    account = find_login_account()
    # subtracting 2 datetime creates a timedelta object
    if account and (account.login_timeout is None or (account.login_timeout - datetime.datetime.now()).days < 0):
        logout_account(account=account)
        return False

    return True


def find_login_account() -> Account:
    logged_in_account = Account.query.filter(Account.login_timeout != '0000-00-00').first()
    return logged_in_account


def update_login_timeout():
    logged_in_account = find_login_account()
    if logged_in_account is None:
        print('tried to extend login, while no account logged')
        return False

    # allow us to extend the timeout in the database
    login_account(logged_in_account)
    return True


def logout_account(account=None):
    if account is not None:
        logged_in_account = account
    else:
        logged_in_account = find_login_account()

    if logged_in_account is not None:
        logged_in_account.login_timeout = None
        logged_in_account.save()


scheduler.add_job(
    check_login_timeout_job,
    'interval',
    seconds=1, id='account-timeout-checker', replace_existing=True
)

# Define the blueprint: 'auth', set its url prefix: app.url/auth
authentication = Blueprint('auth', __name__, url_prefix='/auth')



@authentication.route('/login', methods=['POST'])
def login():
    if 'tag_id' not in request.form:
        # return 'Missing tag_id'
        return make_response('Missing tag_id', 400)

    tag_id = request.form['tag_id']
    found_account = Account.query.filter_by(tag_id=tag_id).first()

    if not found_account:
        return make_response('Didn\'t find tag', 400)

    login_account(found_account)
    socketio.emit('account login', json.dumps(found_account.serialize()))
    return 'Ok'


@authentication.route('/logout')
def logout():
    socketio.emit('account logout')
    return 'Ok'
