from app import db


# Define a base model for other database tables to inherit
class Base(db.Model):
    __abstract__  = True

    id           = db.Column(db.Integer, primary_key=True)
    created_at   = db.Column(db.DateTime,  default=db.func.current_timestamp())
    updated_at   = db.Column(db.DateTime,  default=db.func.current_timestamp(),
                                           onupdate=db.func.current_timestamp())


    def save(self):
        db.session.commit()

# Define a User model
class Account(Base):

    __tablename__ = 'accounts'

    # User Name
    tag_id    = db.Column(db.String(66),  nullable=False, unique=True)

    full_name = db.Column(db.String(60))
    balance   = db.Column(db.Float)

    login_timeout = db.Column(db.DateTime)

    # Identification Data: email & password
    #email    = db.Column(db.String(128),  nullable=False, unique=True)

    # Authorisation Data: role & status

    # New instance instantiation procedure
    def __init__(self, tag_id, full_name, balance = 0):
        self.tag_id     = tag_id
        self.full_name  = full_name
        self.balance    = balance

    def __repr__(self):
        return '<Account %s>' % (self.full_name)

    def serialize(self):
        return {
            'id': self.id,
            'full_name': self.full_name,
            'balance': self.balance
        }